#!/usr/bin/env python
# -*- coding: utf-8 -*-

import asyncio
import logging
import os
import json

import telebot
from aiohttp import web
from telebot import types
from yobit import get_prices, TICKETS

API_TOKEN = os.environ.get('TELEGRAM_TOKEN')
API_URL = 'https://api.telegram.org/bot%s/sendMessage' % API_TOKEN

markup = types.ReplyKeyboardMarkup()
markup.row('BTC/USD', 'LTC/USD', 'ETH/USD')
markup.row('LTC/BTC', 'ETH/BTC', 'ALL')

logger = telebot.logger
telebot.logger.setLevel(logging.INFO)

bot = telebot.TeleBot(API_TOKEN)


async def handler(message):
    logger.info(message.match_info)
    # if message.match_info.get('token') == bot.token:
    data = await message.json()
    update = telebot.types.Update.de_json(data)
    bot.process_new_updates([update])
    return web.Response()
    # else:
    #     return web.Response(status=403)


@bot.message_handler(commands=['start', 'help'])
def send_welcome(message):
    bot.send_message(message.chat.id, "Howdy, how are you doing?", reply_markup=markup)
    logger.info(message)


@bot.message_handler(func=lambda message: True)
def echo_crypto_price(message):
    logger.info(message)
    msg_text = message.text.lower().replace('/', '_')
    ticket = msg_text if msg_text in TICKETS else None
    bot.send_message(message.chat.id, get_prices(ticket), reply_markup=markup)


async def init_app(loop):
    app = web.Application(loop=loop, middlewares=[])
    app.router.add_post('/' + API_TOKEN, handler)
    return app


if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    try:
        app = loop.run_until_complete(init_app(loop))
        web.run_app(app, host='0.0.0.0', port=int(os.environ.get('PORT', 23456)))
    except Exception as e:
        print('Error create server: %r' % e)
    finally:
        pass
    loop.close()

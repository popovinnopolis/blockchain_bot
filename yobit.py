#!/usr/bin/env python
# coding: utf-8

import json

import requests

TICKETS = ('btc_usd', 'eth_btc', 'eth_usd', 'ltc_usd', 'ltc_btc')
YOBIT_API = 'https://yobit.net/api/3/ticker/'

PARAMS_SHOW = ('last', 'last%avg')


def get_prices(ticket=None):
    if ticket is None:
        query = '-'.join(TICKETS)
        tickers = TICKETS
    else:
        query = ticket
        tickers = (ticket,)

    response = requests.get(YOBIT_API + query)
    if response.status_code == 200:
        response = json.loads(response.content)
        result_string = ''
        for curr_ticket in tickers:
            ticket_str = ''
            for param in PARAMS_SHOW:
                if '%' in param:
                    value = (response[curr_ticket][param.split('%')[0]] - response[curr_ticket][param.split('%')[1]]) / \
                            response[curr_ticket][param.split('%')[1]] * 100

                    value = '%+.2f%%' % value
                else:
                    value = '{0:.6g}'.format(response[curr_ticket][param])

                ticket_str += ' %s' % value
            result_string += '%s:%s;\n' % (curr_ticket.replace('_', '/').upper(), ticket_str)
        return result_string
    else:
        return response.content

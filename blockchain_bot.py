#!/usr/bin/env python
# coding: utf-8

import sys

import os
import telebot
import logging
from telebot import types

from yobit import get_prices, TICKETS

markup = types.ReplyKeyboardMarkup()
markup.row('BTC/USD', 'LTC/USD', 'ETH/USD')
markup.row('LTC/BTC', 'ETH/BTC', 'ALL')

TOKEN = os.environ.get('TELEGRAM_TOKEN')
bot = telebot.TeleBot(TOKEN)

logger = logging.getLogger('MyBot')
formatter = logging.Formatter(
    '%(asctime)s (%(filename)s:%(lineno)d %(threadName)s) %(levelname)s - %(name)s: "%(message)s"'
)

console_output_handler = logging.StreamHandler(sys.stdout)
console_output_handler.setFormatter(formatter)
logger.addHandler(console_output_handler)

logger.setLevel(logging.INFO)


@bot.message_handler(commands=['start', 'help'])
def send_welcome(message):
    bot.send_message(message.chat.id, "Howdy, how are you doing?", reply_markup=markup)
    logger.info(message)


@bot.message_handler(func=lambda message: True)
def echo_all(message):
    msg_text = message.text.lower().replace('/', '_')
    ticket = msg_text if msg_text in TICKETS else None
    bot.send_message(message.chat.id, get_prices(ticket), reply_markup=markup)
    logger.info(message)


bot.polling()
